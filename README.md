# Overview

git_update.sh is a bash script performing a punctual synchronization of a git repository. It can either be a new clone or an update.  
**CAREFUL**: git_update.sh is not working-directory safe and can delete your work.
```
git_update.sh -d ~/website -r V3 $REMOTE_URL 
```
Each time this command is called, the ~/website directory is resync with corresponding git branch or tag V3.

# Installation

Download the git_update.sh file. You can place it in /usr/local/bin.  
Dependencies: Bash and Git 

# Use

## Help extract

```
NAME
    git_update.sh
SYNOPSIS
    git_update.sh [-h] [-r ref] [-d dest] [-H] [-a] repository
OPTIONS
	-h prints the help. 
	-r specifies the reference to the commit to be synchronized. It can be a tag or a branch. By default, it is
		the last commit of branch main.
		It can be different from the actual branch. CAREFUL, the command git branch will always show the original branch name even though a branch switch has happened.
	-d specifies the destination of the clone or update. Directory must be empty if a new clone is to be made.
		If the repository to be cloned is local, and its path is passed as a relative path, the path should start from the destination. 
		To avoid mistakes, absolute paths are advised. 
	-H allows the $HOME directory to be used by git_update.sh. By default, git_update.sh cannot access $HOME to
		prevent default behavior. 
		If you need the global .gitconfig located in your $HOME to be used, you should supply the -H option. 	
	-a specifies that the aggressive option of the git garbage collection must be used. Only advised when
		changes happen in many different objects. Will slow down the execution.   
DESCRIPTION
	This script will replace the destination with the wanted commit of a git repository. The history is not preserved but tags are. Untracked files remain.
	The git commands have been chosen so as to minimize the memory and bandwidth usages.
```

## Cloning

git_update.sh will only clone in an empty repository, by default the current working directory. 
If called in a repository with a .git directory, it will update (see below). If called in a repository with untracked files, it will fail. 
Only the commit and the necessary objects will be cloned. The commit can be indicated through a reference, either a tag or branch, otherwise the last commit of main is the default.  
**CAREFUL**: the command git branch will always show the original branch name and will not follow the eventual branch changes. 
git_update.sh clones using  
`git clone --recurse-submodules --shallow-submodules --depth=1`

## Updating

git_update.sh will update if the repository already contains a .git. Untracked files and directories will be kept. Any local modification to a tracked file or created tracked file will be deleted in favor of the new commit. The history will only contain the very last commit. 
git_update.sh updates using 
```
git fetch --tags --depth=1 --prune --prune-tags --force origin $ref 
git reset --hard --recurse-submodules FETCH_HEAD 
git submodule update --init --recursive --force --depth=1 --remote 
git reflog expire --expire=now --all 
git gc --prune=now [--aggressive]"
```
## Examples

**A) Cloning the last commit of a branch into a non-existing directory**  
`git_update.sh -r myBranch -d myDirectory $REMOTE_URL`  
**Result:** a directory myDirectory has been created in the current working directory. It is filled with the files of the last commit of branch myBranch as well as the .git. The history only shows the last commit.

**B) Updating the current working directory repository, moving it from its current state to a certain tag, calling for an aggressive garbage collection.**  
`git_update.sh -r myTag -a $REMOTE_URL`  
**Result:** untracked files are still here. all the tracked files have been changed to respect the tag commit. The history only shows the last commit.  
git gc has been called with the --aggressive option, hence the objects in the .git have been repacked so as to optimize memory.  
CAREFUL: git branch still displays the original branch name and not the one of the tag. 

# Development process

git_update.sh has been written by the French association [Jean-Cloud](https://www.jean-cloud.net), in the process of developing Shlagernetes, an orchestration tool. 
Shlagernetes allows storing services on fallible second-hand servers and tries to consume the less possible resources. 

Several tests have been performed in order to find the most energy-saving Git commands. 
You can find them under [/test/performance_tests](./test/performance_tests).
Please refer to [/doc/development_explanations](./doc/development_explanations) for more information about the tests and choices made in the elaboration of git_update.sh.

# Testing

The script test_git_update.sh can be found at [test/functional tests](./test/functional_tests). It will create a repository to test on before testing.  
Please refer to the readme of test [/test/README.md](./test/README.md) and the readme of functional_tests [/test/functional_tests/README.md](./test/functional_tests/README.md). 

# How to contribute

If you have any suggestion or question about git_update.sh, please contact us at contact@jean-cloud.org. 
We would be delighted to hear your feedback. Feel free to send diff files with improvement suggestions.

# Introduction

performance_tests contains tests who compare the memory and bandwidth usage of different Git commande in order to compare them.
functional_tests contains tests who verify that git_update.sh is behaving as expected.
Both rely on the same test repository that is created by test_repo_creation.sh. 
Both have to be run from their directory because they rely on relative paths.

# Creation of the test repository

The script test_repo_creation.sh creates a remote/$REPO_NAME repository. 

## Help extract

```
NAME
    test_repo_creation.sh
SYNOPSIS
    test_repo_creation.sh [-h] [-l] [-s]
DESCRIPTION
        This script creates a ./remote directory in the current directory, then creates a remote/$REPO_NAME git repository. 
        This git repository is filled with randomly generated binary files described in the readme.md.
OPTIONS
    -h prints the help. 
    -l [link] adds the linked online repository as remote and pushes changes to it. Must be en empty repository.
    -s creates a submodule remote/$SUB_NAME and includes it in remote/$REPO_NAME. 
```

# Description of the created repository

Here is a history of the commits:

### branch main 			

commit  sample0 1M created
commit  sample1 1M created
commit  sample3 5M created
commit  sample4 1M created
commit  sample3 5M deleted (tag : tagging_point)
[if  -s is selected] commit adding $SUB_NAME module

### branch secondary

commit  sample0 1M created 
commit  sample1 1M created
commit  sample2 500K created

Which gives the latest state:

### branch main

sample0	1M
sample1	1M
sample4	1M

### branch secondary

sample0 1M
sample1 1M
sample2 500K

If the -s option is selected, the submodule is created, and integrated in the last commit of main, with this history of commits:

### branch main

commit  first 1M sample created

which gives the latest state:

### branch main

sub_sample0
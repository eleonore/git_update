#!/bin/bash
Help()
{
   echo "
NAME
    test_repo_creation.sh
SYNOPSIS
    test_repo_creation.sh [-h] [-l] [-s]
DESCRIPTION
        This script creates a ./remote directory in the current directory, then creates a remote/$REPO_NAME git repository. 
        This git repository is filled with randomly generated binary files described in the readme.md.
OPTIONS
    -h prints the help. 
    -l [link] adds the linked online repository as remote and pushes changes to it. Must be en empty repository.
    -s creates a submodule remote/$SUB_NAME and includes it in remote/$REPO_NAME. "
}

create_random_file(){
    dd if=/dev/urandom of=$1 bs=$2 count=1 &> /dev/null
}

REPO_NAME=git_update_testing
REPO_PATH=./remote
WITH_SUBMODULE="false"
SUB_NAME="submodule"
WITH_LINK="false"
while getopts ":hn:sl:" option; do
   case $option in
      h) 
         Help
         exit;;
      n)
        REPO_NAME=$OPTARG;;
      s) 
        WITH_SUBMODULE="true";;
      l)
        WITH_LINK="true"
        LINK=$OPTARG;;
     \?)
         echo "Error: Invalid option"
         exit;;
   esac
done

if [ ! -d $REPO_PATH ]; then
    mkdir $REPO_PATH
fi
cd $REPO_PATH
if [ ! -d $REPO_NAME ]; then
    echo "remote/$REPO_NAME will be created"
    mkdir $REPO_NAME
    cd $REPO_NAME
    git init
    git branch -m main
    create_random_file 'sample0' '1M'
    git add .
    git commit -m"first 1M sample created"
    create_random_file 'sample1' '1M'
    git add sample1 
    git commit -m"second 1M sample created"
    git branch secondary
    git checkout secondary
    create_random_file 'sample2' '500K'
    git add sample2  
    git commit -m"first 500K sample created in branch secondary"
    git checkout main
    create_random_file 'sample3' '5M'
    git add sample3
    git commit -m"first 5M sample created"
    git tag tagging_point
    create_random_file 'sample4' '1M'
    git add sample4
    git commit -m"third 1M sample created"
    rm sample3
    git add sample3
    git commit -m"sample3 deleted"
    cd ..
    if [ "$WITH_SUBMODULE" = "true" ]; then        
        mkdir $SUB_NAME
        cd $SUB_NAME
        git init
        git branch -m main
        create_random_file 'sub_sample0' '1M'
        git add .
        git commit -m"first 1M sample created"
        cd ../$REPO_NAME
        git submodule add ../$SUB_NAME
        git commit -am "adding $SUB_NAME module"
    fi
    cd ..
else # $REPO_NAME exists
    echo "remote/$REPO_NAME already exists"
    if [[ "$WITH_SUBMODULE" = "true" && ! -d $SUB_NAME ]]; then        
        mkdir $SUB_NAME
        cd $SUB_NAME
        git init
        git branch -m main
        create_random_file 'sub_sample0' '1M'
        git add .
        git commit -m"first 1M sample created"
        cd ../$REPO_NAME
        git submodule add ../$SUB_NAME
        git commit -am "adding $SUB_NAME module"
    elif [[ "$WITH_SUBMODULE" != "true" && -d $SUB_NAME ]]; then
        cd $REPO_NAME
        git reset --hard HEAD~1 &> /dev/null
        rm -rf $SUB_NAME &> /dev/null
        cd ..
        rm -rf $SUB_NAME
    fi
fi
if [ "$WITH_LINK" = "true" ]; then
    cd $REPO_NAME
    if [ $(git remote | grep "origin" | wc -l) = 0 ]; then 
        echo "adding origin"
        git remote add origin $LINK
        git push origin -u --all 
        git push origin -u --tags
    fi
fi

cd ..


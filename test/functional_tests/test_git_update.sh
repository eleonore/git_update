#! /bin/bash
echo "<execution of test_git_update.sh>"
Help()
{
   echo "
NAME
    test_git_update.sh
SYNOPSIS
    test_git_update.sh [-a] [-h] [-c] [-n number]
OPTIONS
    -a excutes all the tests and cleans.
    -n number executes test number.
    -c cleans.
    -h prints the help. 
DESCRIPTION
    TEST0: git cloned in an empty directory
    TEST1: git cloned in an empty directory with tag
    TEST2: git cloned in an empty directory with branch
    TEST3: git updated fast-forward on main
    TEST4: git updated fast-forward on main with untracked file
    TEST5: git updated with underlying conflict on main
    TEST6: git updated, switching to another branch, deleting and adding files in the process
    TEST7: git updated, switching to a tag, deleting and adding files in the process
    TEST8: git updated, before and after changing a tag, deleting and adding files in the process
    TEST9: git updated fast-forward on submodule on main"
}

#Variables definitions
WORKING_DIRECTORY=$(pwd)
REPO_NAME="git_update_testing"
SUBMODULE_NAME="submodule"
REMOTE="file://${WORKING_DIRECTORY}/remote/$REPO_NAME" #we must trick git_update.sh into thinking we are not working in local, otherwise depth=1 is not respected
LOCAL_REMOTE="${WORKING_DIRECTORY}/remote/$REPO_NAME" #cd does not understand file://
LOCAL_REMOTE_SUBMODULE="/${WORKING_DIRECTORY}/remote/$SUBMODULE_NAME"
FILENAMES=("sample0" "sample1 sample4") 
FILENAMES_TAG=("sample0 sample1 sample3")
FILENAMES_BRANCH=("sample0 sample1 sample2") 
FILE_ON_BRANCH_ONLY="sample2"
FILE_ON_TAG_ONLY="sample3"
FILE_ON_MAIN_ONLY="sample4"
FILE_TO_BE_DELETED="sample0" 
FILE_TO_BE_DELETED_SUBMODULE="sub_sample0"
FILE_TO_BE_CREATED="new_file"
FILE_TO_BE_MODIFIED=$FILE_TO_BE_CREATED
UNTRACKED_FILE="untracked_file"
TAG_NAME="tagging_point"
TAG_FLAG="tag_flag"
BRANCH_NAME="secondary"

#preparing the test repository
bash ../test_repo_creation.sh -s &> /dev/null

#output function
section(){
    echo "------------------------------ $1 ------------------------------"
}

#clean
clean(){
    if [ -d remote ]; then
        rm -rf remote
    fi
    if [ -d $REPO_NAME ]; then
        rm -rf $REPO_NAME
    fi
    echo "<cleaned>"
}

#check functions
cloning_check(){
    local input=("$@")
    local repo_name=${input[0]}
    local FILENAMES=${input[@]:1}
    if  [[ -d "./$repo_name" ]]; then
        echo "into $repo_name"
        cloning_result=0
        for FILE in ${FILENAMES[@]}
        do  
            if [ -f "./$repo_name/$FILE" ]; then 
                echo "the file $FILE has correctly been imported"
            else 
                echo "the file $FILE could not be imported"
                cloning_result=1
            fi
        done  
    else
        cloning_result=1
        echo "the directory $repo_name could not be created"
    fi
}

history_check(){
    local repo_name=$1
    cd $repo_name
    history_result=0
    if [ $(git log | grep "Date" | wc -l) -gt 1 ]; then
        echo "Several commits have been saved"
        history_result=1
    elif [ $(git log | grep "Date" | wc -l) = 1 ]; then
        echo "Only the last commit has been saved"
    else
        history_result=1
        echo "Cloning error, incoherent git log"
    fi
    cd ..
}

tag_check(){
    local repo_name=$1
    cd $repo_name
    git branch > res
    if [ $(grep "no branch" res | wc -l) = 1 ]; then
        echo "The tag instruction has been respected"
        tag_result=0
    else
        echo "The tag instruction has not been respected"
        tag_result=1
    fi
    cd ..
}

branch_check(){
    local repo_name=$1
    cd $repo_name
    git branch > res
    if [ $(grep $BRANCH_NAME res | wc -l) = 1 ];then
        echo "The branch instruction has been respected"
        branch_result=0
    else
        branch_result=1
        echo "The branch instruction has not been respected"
    fi
    cd ..
}

untracked_check(){
    local repo_name=$1
    cd $repo_name
    if [ -f $UNTRACKED_FILE ]; then
        echo "The untracked file is still here."
        untracked_result=0
        cd ..
        conflict_check $repo_name
        untracked_result=conflict_result
    else
        echo "The untracked file has been deleted."
        cd ..
        untracked_result=1
    fi
}

modification_check(){
    local repo_name=$1
    cd $repo_name
    if [ -f $FILE_TO_BE_CREATED ]; then
        echo "The new file has been imported."
        modification_result=0
        
    else
        echo "The new file has not been imported."
        modification_result="1"
    fi
    if [ -f "$FILE_TO_BE_DELETED" ]; then
        echo "$FILE_TO_BE_DELETED has not been deleted."
        modification_result="1"
    else    
        echo "$FILE_TO_BE_DELETED has been deleted."
    fi
    cd ..
}

modification_submodule_check(){
    local repo_name=$1
    cd $repo_name/$SUBMODULE_NAME
    if [ -f $FILE_TO_BE_CREATED ]; then
        echo "The new file has been imported."
        modification_submodule_result=0
        
    else
        echo "The new file has not been imported."
        modification_submodule_result="1"
    fi
    if [ -f "$FILE_TO_BE_DELETED_SUBMODULE" ]; then
        echo "$FILE_TO_BE_DELETED_SUBMODULE has not been deleted."
        modification_submodule_result="1"
    else    
        echo "$FILE_TO_BE_DELETED_SUBMODULE has been deleted."
    fi
    cd ../..
}

switching_branch_check(){
    local repo_name=$1
    cd $repo_name
    if [ -f $FILE_ON_BRANCH_ONLY ]; then
        echo "The files of the branch $BRANCH_NAME are present."
        branch_switching_result=0
            if [ -f $FILE_ON_MAIN_ONLY ]; then
                echo "The files of the branch main are present."
                branch_switching_result=1
            else
                echo "The files of the branch main are absent."
                branch_switching_result=0
            fi
    else
        echo "The files of the branch $BRANCH_NAME are absent."
        branch_switching_result=1
    fi
    cd ..
}

switching_tag_check(){
    local repo_name=$1
    cd $repo_name
    if [ -f $FILE_ON_TAG_ONLY ]; then
        echo "The files of the tag $TAG_NAME are present."
        tag_switching_result=0
            if [ -f $FILE_ON_MAIN_ONLY ]; then
                echo "The files of the last commit on main are present."
                tag_switching_result=1
            else
            echo "The files of the last commit in main are absent."
                tag_switching_result=0
            fi
    else
        echo "The files of the tag $TAG_NAME are absent."
        tag_switching_result=1
    fi
    cd ..
}

conflict_check(){
    local repo_name=$1
    cd $repo_name
    if [[ $(grep "new" $FILE_TO_BE_MODIFIED | wc -l) > 0 && $(grep "different" $FILE_TO_BE_MODIFIED | wc -l) = 0 ]]; then
        echo "The remote version has overwritten the local version."
        conflict_result=0
    else
        echo "The remote version has not overwritten the local version."
    fi
    cd ..
}

#intermediate functions
modification_remote(){
    cd $LOCAL_REMOTE
    touch $FILE_TO_BE_CREATED
    echo "new text" > $FILE_TO_BE_CREATED
    rm $FILE_TO_BE_DELETED
    git add $FILE_TO_BE_CREATED $FILE_TO_BE_DELETED &> /dev/null
    git commit -m "$FILE_TO_BE_CREATED created and $FILE_TO_BE_DELETED deleted" &> /dev/null
    cd ../..
}


undo_modification_remote(){
    cd $LOCAL_REMOTE
    git revert --no-edit HEAD &> /dev/null
    cd ../..
}

modification_submodule(){
    cd $LOCAL_REMOTE_SUBMODULE
    touch $FILE_TO_BE_CREATED
    echo "new text" > $FILE_TO_BE_CREATED
    rm $FILE_TO_BE_DELETED_SUBMODULE
    git add $FILE_TO_BE_CREATED $FILE_TO_BE_DELETED_SUBMODULE &> /dev/null
    git commit -m "$FILE_TO_BE_CREATED created and $FILE_TO_BE_DELETED_SUBMODULE deleted" &> /dev/null
    cd ../..
}

undo_modification_submodule(){
    cd $LOCAL_REMOTE_SUBMODULE
    git revert --no-edit HEAD &> /dev/null
    cd ../..
}

add_untracked_file(){
    local repo_name=$1
    cd $repo_name
    touch $UNTRACKED_FILE 
    cd ..
}

remove_untracked_file(){
    local repo_name=$1
    cd $repo_name
    touch $UNTRACKED_FILE 
    cd ..
}

modification_local(){
    local repo_name=$1
    cd $repo_name
    touch $FILE_TO_BE_CREATED
    echo "different text" > $FILE_TO_BE_CREATED
    git add $FILE_TO_BE_CREATED &> /dev/null
    git commit -m "$FILE_TO_BE_CREATED added" &> /dev/null
    cd ..
}

changing_tag(){
    cd $LOCAL_REMOTE
    git checkout $TAG_NAME &> /dev/null 
    git tag -f $TAG_FLAG &> /dev/null #temporary tag flag to be able to put the tag back here after the test
    git checkout $BRANCH_NAME &> /dev/null 
    git tag -f $TAG_NAME &> /dev/null #moving the tag on the branch secondary
    cd ../..
}

undo_changing_tag(){
    cd $LOCAL_REMOTE
    git checkout $TAG_FLAG &> /dev/null
    git tag -f $TAG_NAME &> /dev/null #move locally
    git tag --delete $TAG_FLAG &> /dev/null #delete locally
    git checkout main &> /dev/null
    cd ../..
}


test0 (){
#CASE 0: git cloned in an empty directory
    section TEST0
    #if it exists, delete the directory
    if [ -d $REPO_NAME ]; then
        rm -rf $REPO_NAME
    fi
    ../../src/git_update.sh -d $REPO_NAME $REMOTE &> /dev/null
    #checks
    cloning_check $REPO_NAME ${FILENAMES[@]}
    history_check $REPO_NAME
    
    case0=$((history_result+cloning_result))
    if [ "$case0" = "0" ]; then
        echo "case 0, in a empty directory: OK"
    else
        echo "case 0, in a empty directory: FAIL"
    fi
}

test1(){
#CASE 1: git cloned in an empty directory with tag
    section TEST1
    #if it exists, delete the directory
    if [ -d $REPO_NAME ]; then
        rm -rf $REPO_NAME
    fi
    ../../src/git_update.sh -d $REPO_NAME -r $TAG_NAME $REMOTE &> /dev/null
    #checks
    cloning_check $REPO_NAME ${FILENAMES_TAG[@]}
    history_check $REPO_NAME
    tag_check $REPO_NAME

    case1=$((cloning_result+history_result+tag_result))
    if [ $case1 = 0 ]; then
        echo "case 1, in a empty directory, with tag: OK"
    else
        echo "case 1, in a empty directory, with tag: FAIL"
    fi
}

test2(){
#CASE 2: git cloned in an empty directory with branch
    section TEST2
    #if it exists, delete the directory
    if [ -d $REPO_NAME ]; then
        rm -rf $REPO_NAME
    fi
    ../../src/git_update.sh -d $REPO_NAME -r $BRANCH_NAME $REMOTE &> /dev/null
    #checks
    cloning_check $REPO_NAME ${FILENAMES_BRANCH[@]}
    history_check $REPO_NAME
    branch_check $REPO_NAME

    case2=$((cloning_result+history_result+branch_result))
    if [ $case2 = 0 ]; then
        echo "case 2, in a empty directory, with branch: OK"
    else
        echo "case 2, in a empty directory, with branch: FAIL"
    fi
}

test3(){
#CASE 3: git updated fast-forward on main
    section TEST3
    #if it exists, delete the directory
    if [ -d $REPO_NAME ]; then
        rm -rf $REPO_NAME
    fi
    #clone the repo in its last state
    git clone --recurse-submodules --shallow-submodules --depth 1 $REMOTE &> /dev/null
    #modify the remote
    modification_remote
    #make git_update.sh update the repository
    cd $REPO_NAME
    ../../../src/git_update.sh $REMOTE &> /dev/null
    cd ..
    #checks
    modification_check $REPO_NAME
    history_check $REPO_NAME
    #cleaning
    undo_modification_remote

    case3=$((modification_result+history_result))
    if [ "$case3" = "0" ]; then
        echo "case 3, fast-forward update on main: OK"
    else
        echo "case 3, fast-forward update on main: FAIL"
    fi
}

test4(){
#CASE 4: git updated fast-forward on main with untracked file
    section TEST4 
    #if it exists, delete the directory
    if [ -d $REPO_NAME ]; then
        rm -rf $REPO_NAME
    fi
    #clone the repo in its last state
    git clone --recurse-submodules --shallow-submodules --depth 1 $REMOTE &> /dev/null
    #modify the remote
    modification_remote
    #add an untracked file
    add_untracked_file $REPO_NAME
    #make git_update.sh update the repository
    cd $REPO_NAME
    ../../../src/git_update.sh $REMOTE &> /dev/null
    cd ..
    #checks
    modification_check $REPO_NAME
    history_check $REPO_NAME
    untracked_check $REPO_NAME
    #cleaning
    undo_modification_remote 
    remove_untracked_file $REPO_NAME

    case4=$((modification_result+history_result+untracked_result))
    if [ "$case4" = "0" ]; then
        echo "case 4, fast-forward update on main with untracked file: OK"
    else
        echo "case 4, fast-forward update on main with untracked file: FAIL"
    fi
}

test5(){
#CASE 5: git updated with underlying conflict on main
    section TEST5
    #if it exists, delete the directory
    if [ -d $REPO_NAME ]; then
        rm -rf $REPO_NAME
    fi
    #clone the repo in its last state
    git clone --recurse-submodules --shallow-submodules --depth 1 $REMOTE &> /dev/null
    #modify the local repo
    modification_local $REPO_NAME
    #modify the remote
    modification_remote
    #make git_update.sh update the local repository
    cd $REPO_NAME
    ../../../src/git_update.sh $REMOTE &> /dev/null
    cd ..
    #checks
    conflict_check $REPO_NAME
    #cleaning
    undo_modification_remote

    case5=$((conflict_result+history_result))
    if [ "$case5" = "0" ]; then
        echo "case 5, update with underlying conflict: OK"
    else
        echo "case 5, update with underlying conflict: FAIL"
    fi
}

test6(){
#CASE 6: git updated, switching to another branch, deleting and adding files in the process
    section TEST6
    #if it exists, delete the directory
    if [ -d $REPO_NAME ]; then
        rm -rf $REPO_NAME
    fi
    #clone the repo in its last state
    git clone --recurse-submodules --shallow-submodules --depth 1 $REMOTE &> /dev/null
    #call git update with another branch
    cd $REPO_NAME
    ../../../src/git_update.sh -r $BRANCH_NAME $REMOTE &> /dev/null
    cd ..
    #checks
    switching_branch_check $REPO_NAME
    history_check $REPO_NAME
    case6=$((branch_switching_result+history_result))
    if [ "$case6" = "0" ]; then
        echo "case 6, branch-switching update: OK"
    else
        echo "case 6, branch-switching update: FAIL"
    fi
}

test7(){
#CASE 7: git updated, switching to a tag, deleting and adding files in the process
    section TEST7
    #if it exists, delete the directory
    if [ -d $REPO_NAME ]; then
        rm -rf $REPO_NAME
    fi
    #clone the repo in its last state
    git clone --recurse-submodules --shallow-submodules --depth 1 $REMOTE &> /dev/null
    #call git_update.sh with a tag
    cd $REPO_NAME
    ../../../src/git_update.sh -r $TAG_NAME $REMOTE &> /dev/null
    cd ..
    #checks
    switching_tag_check $REPO_NAME
    history_check $REPO_NAME
    case7=$((tag_switching_result+history_result))
    if [ "$case7" = "0" ]; then
        echo "case 7, tag-switching update: OK"
    else
        echo "case 7, tag-switching update: FAIL"
    fi
}

test8(){
#CASE 8: git updated, before and after changing a tag, deleting and adding files in the process
    section TEST8
    #if it exists, delete the directory
    if [ -d $REPO_NAME ]; then
        rm -rf $REPO_NAME
    fi
    #clone the repo in its last state
    git clone --recurse-submodules --shallow-submodules --depth 1 $REMOTE &> /dev/null
    #call git_update.sh with a tag
    cd $REPO_NAME
    ../../../src/git_update.sh -r $TAG_NAME $REMOTE &> /dev/null
    cd ..
    #intermediate check to make sure that we are starting on the initial position of the tag
    echo "first position of the tag"
    switching_tag_check $REPO_NAME
    #change the position of the tag
    changing_tag
    #call git_update.sh again to go to the new position of the tag
    cd $REPO_NAME
    ../../../src/git_update.sh -r $TAG_NAME $REMOTE &> /dev/null
    cd ..
    #put back the remote in its initial state
    undo_changing_tag

    #checks
    echo "second position of the tag"
    switching_branch_check $REPO_NAME #new position of the tag=last commit of the branch hence we can reuse this check
    history_check $REPO_NAME
    case8=$((tag_switching_result+branch_switching_result+history_result))
    if [ "$case8" = "0" ]; then
        echo "case 8, tag-changing update: OK"
    else
        echo "case 8, tag-changing update: FAIL"
    fi
}

test9(){
#CASE 9: git updated fast-forward on submodule on main
    section TEST9
    #if it exists, delete the directory
    if [ -d $REPO_NAME ]; then
        rm -rf $REPO_NAME
    fi
    #clone the repo in its last state
    git clone --recurse-submodules --shallow-submodules --depth 1 $REMOTE &> /dev/null
    #modify the remote
    modification_submodule
    #make git_update.sh update the repository
    cd $REPO_NAME
    ../../../src/git_update.sh $REMOTE &> /dev/null
    cd ..
    #checks
    modification_submodule_check $REPO_NAME
    history_check $REPO_NAME
    #cleaning
    undo_modification_submodule

    case9=$((modification_submodule_result+history_result))
    if [ "$case9" = "0" ]; then
        echo "case 9, fast-forward update on submodule on main: OK"
    else
        echo "case 9, fast-forward update on submodule on main: FAIL"
    fi
} 

ALL_TESTS="false"
TEST_NUM=""
while getopts ":hcn:a" option; do
   case $option in
      h) # display Help
            Help
            exit;;
      c) #clean
            clean;;
      n) #perform specific test
            TEST_NUM=$OPTARG;;
      a) #perform all tests and clean
            ALL_TESTS="true";; 
     \?) # Invalid option
            echo "Error: Invalid option"
            Help
            exit;;
   esac
done

if [ "$ALL_TESTS" = "true" ]; then
    test0
    test1 
    test2 
    test3 
    test4 
    test5 
    test6 
    test7 
    test8 
    test9
    clean
elif [ -n "$TEST_NUM" ]; then
    #in order to only create the temporary clone once if we execute all tests
    case $TEST_NUM in
        0) 
            test0;;
        1)
            test1;;
        2) 
            test2;;
        3) 
            test3;;
        4) 
            test4;;
        5) 
            test5;;
        6)
            test6;;
        7) 
            test7;;
        8) 
            test8;;
        9)
            test9;;
        *)
            echo "Error: Invalid test number"
            exit;;
    esac
fi
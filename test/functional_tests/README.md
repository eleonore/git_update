## Introduction 

test_git_update.sh performs functional tests on git_update.sh to verify that it is behaving as planned. 

## Tests

The script consists in nine tests. The three first ones test the cloning and the following test the updating of git_update.sh.

Each test consists of calling git_update.sh, eventually after some modifications, and compare the result with the expected result. The comparison is done by *_check functions, which modify each a boolean *_result.
If all booleans of checks conducted are true, then the boolean case* is also true and the test is a success.

## Help extract

```
NAME
    test_git_update.sh
SYNOPSIS
    test_git_update.sh [-a] [-h] [-c] [-n number]
OPTIONS
    -a excutes all the tests and cleans.
    -n number executes test number.
    -c cleans.
    -h prints the help. 
DESCRIPTION
    TEST0: git cloned in an empty directory
    TEST1: git cloned in an empty directory with tag
    TEST2: git cloned in an empty directory with branch
    TEST3: git updated fast-forward on main
    TEST4: git updated fast-forward on main with untracked file
    TEST5: git updated with underlying conflict on main
    TEST6: git updated, switching to another branch, deleting and adding files in the process
    TEST7: git updated, switching to a tag, deleting and adding files in the process
    TEST8: git updated, before and after changing a tag, deleting and adding files in the process
    TEST9: git updated fast-forward on submodule on main"
```

## Debugging

In case of problem, it is adviced to run test_git_update.sh -c to clean the repository. 
This will force test_repo_creation.sh to run again and provide a new test repository.
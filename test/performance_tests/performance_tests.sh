#!/bin/bash
echo "<execution of performance_tests.sh>"
Help()
{
   echo "
NAME
    performance_tests.sh
SYNOPSIS
    performance_tests.sh [-a] [-h] [-n number]
OPTIONS
    -a executes all the tests.
    -n number executes test number.
    - c cleans.
    -h prints the help. 
DESCRIPTION
    This script is in writing. It allows you to measure memory and bandwidth usage. The first four test different cloning methods. Te following apply changes to the local remote before testing fetching and merging commands.
    TEST0: classic cloning 
    TEST1: --single-branch cloning
    TEST2: --depth=1 --no-single-branch cloning
    TEST3: --depth=1 cloning
    TEST4: --depth=1 with reflog and gc cloning
    TEST5: sparse-checking 1M sample0 cloning
    _________________
    TEST6: classic fetching+checking out after addition of a 1M file
    TEST7: classic fetching+checking out after removal of a 1M file
    TEST8: classic fetching+checking out after addition then removal of a 1M file

    TEST9: --depth=1 fetching+checking out after addition of a 1M file
    TEST10: --depth=1 fetching+checking out after removal of a 1M file
    TEST11: --depth=1 fetching+checking out after addition then removal of 1M a file

    TEST12: --depth=1 fetching+checking out with reflog annd gc after addition of a 1M file
    TEST13: --depth=1 fetching+checking out with reflog annd gc after removal of a 1M file
    TEST14: --depth=1 fetching+checking out with reflog annd gc after addition then removal of a 1M file

    TEST15: --depth=1 fetching+ --reset=hard after addition of a 1M file
    TEST16: --depth=1 fetching+ --reset=hard after removal of a 1M file
    TEST17:  --depth=1 fetching+ --reset=hard after addition then removal of a 1M file

    TEST18: --depth=1 fetching+ --reset=hard and reflog and gc after addition of a 1M file
    TEST19: --depth=1 fetching+ --reset=hard and reflog and gc after removal of a 1M file
    TEST20: --depth=1 fetching+ --reset=hard and reflog and gc after addition then removal of a 1M file

    TEST21: --depth=1 fetching+checking out after addition of a 1M file in submodule
    TEST22: --depth=1 fetching+checking out after removal of a 1M file in submodule
    TEST23: --depth=1 fetching+checking out after addition then removal of a 1M file in submodule 

    TEST24: --depth=1 fetching+merging -X theirs with reflog and gc after addition of a 1M file
    TEST25: --depth=1 fetching+merging -X theirs with reflog and gc after removal of a 1M file
    TEST26: --depth=1 fetching+merging -X theirs with reflog and gc after addition then removal of a 1M file

    TEST27: --depth=1 fetching+merging -s ours with reflog and gc after addition of a 1M file
    TEST28: --depth=1 fetching+merging -s ours with reflog and gc after removal of a 1M file
    TEST29: --depth=1 fetching+merging -s ours with reflog and gc after addition then removal of a 1M file"
}

REPO_NAME=git_update_testing
REPO_PATH=./remote
WITH_SUBMODULE="true"
SUB_NAME="submodule_for_performance_testing"
FILES_TO_KEEP='sample0'
REMOTE="./remote/performance_testing"

#preparing the test repository
if [ "$WITH_SUBMODULE" = "true" ]; then
    bash test_repo_creation.sh -s &> /dev/null
else 
    bash test_repo_creation.sh &> /dev/null
fi

#output function
section(){
    echo "------------------------------ $1 ------------------------------"
}

#clean
clean(){
    if [ -d remote ]; then
        rm -rf remote
    fi
    if [ -d $REPO_NAME ]; then
        rm -rf $REPO_NAME
    fi
    echo "<cleaned>"
}

#useful functions for the tests
create_random_file(){
    dd if=/dev/urandom of=$1 bs=$2 count=1 &> /dev/null
}

get_storage_used(){
    mem=$(du $1 | tail -n1 | tr -cd [:digit:])
}

get_bandwidth(){
    bw="unknown"
    bw=$(grep -e "Receiving objects:" $1 | grep -o "Receiving objects: [[:alnum:]%/(),. ]*" | tail -n1)
    bw=${bw#*,}
}

#Tests on the initial populating of the repository
section "Tests on the initial populating of the repository"
test0(){
    section TEST0
    echo "TEST 0: classic cloning."
    git clone --recurse-submodules --remote-submodules --progress --no-local $1 &> cloning_text
    cd $REPO_NAME
    git submodule update --init --recursive --force --remote --progress &> /dev/null
    cd ..
    get_storage_used "./$REPO_NAME" 
    get_bandwidth cloning_text
    echo "memory usage: $mem"
    echo "bandwidth usage (submodule excluded): $bw"    
    rm cloning_text
    rm -rf $REPO_NAME 
}

test1(){
    section TEST1
    echo "TEST 1: --single-branch cloning."
    git clone --recurse-submodules --remote-submodules --progress --single-branch --no-local $1 &> cloning_text
    cd $REPO_NAME
    git submodule update --init --recursive --force --remote --progress &> /dev/null
    cd ..
    get_storage_used ./$REPO_NAME
    get_bandwidth cloning_text
    echo "memory usage: $mem"
    echo "bandwidth usage (submodule excluded): $bw"
    rm cloning_text
    rm -rf $REPO_NAME
}

test2(){
    section TEST2
    echo "TEST 2: --depth=1 --no-single-branch"
    git clone --recurse-submodules --remote-submodules --progress --depth=1 --no-local --no-single-branch $1 &> cloning_text
    cd $REPO_NAME
    git submodule update --init --recursive --force --remote --progress --depth=1 --no-single-branch &> /dev/null
    cd ..
    get_storage_used ./$REPO_NAME
    get_bandwidth cloning_text
    echo "memory usage: $mem"
    echo "bandwidth usage (submodule excluded): $bw"
    rm cloning_text
    rm -rf $REPO_NAME
}

test3(){
    section TEST3
    echo "TEST 3: --depth=1 with single-branch (default))"
    git clone --recurse-submodules --remote-submodules --progress --single-branch --no-local --depth=1 $1 &> cloning_text
    cd $REPO_NAME
    git submodule update --init --recursive --force --remote --progress --depth=1 &> /dev/null
    cd ..
    get_storage_used ./$REPO_NAME
    get_bandwidth cloning_text
    echo "memory usage: $mem"
    echo "bandwidth usage (submodule excluded): $bw"
    rm cloning_text
    rm -rf $REPO_NAME
}

test4(){
    section TEST4
    echo "TEST 4: --depth=1 with single-branch (default) and reflog and gc"
    git clone --recurse-submodules --remote-submodules --progress --single-branch --no-local --depth=1 $1 &> cloning_text
    cd $REPO_NAME
    git submodule update --init --recursive --force --remote --progress --depth=1 &> /dev/null
    git reset --hard
    cd ..
    get_storage_used ./$REPO_NAME
    get_bandwidth cloning_text
    echo "memory usage: $mem"
    echo "bandwidth usage (submodule excluded): $bw"
    rm cloning_text
    rm -rf $REPO_NAME
}

test5(){
    section TEST5
    mkdir $REPO_NAME
    echo "TEST 5 : sparse-checking only $FILES_TO_KEEP with depth=1"
    #creating a git repo with sparse-checking settings
    cd $REPO_NAME
    git init -q
    git config core.sparsecheckout true 
    echo $FILES_TO_KEEP >> .git/info/sparse-checkout 
    #pulling from the remote with sparse-checking enabled
    git remote add -f origin ../$1 &> /dev/null
    git submodule update --init --recursive --force --depth=1 --remote &> /dev/null
    git fetch --progress --tags --depth=1 --prune --prune-tags origin &> /dev/null
    git checkout -f origin/main &> /dev/null
    get_storage_used .
    echo "memory usage: $mem"
    echo "bandwidth usage (submodule excluded): unknown"
    cd ..
    rm -rf $REPO_NAME
}

#Tests on the updating of the repository
#classic fetching
test6(){
    section TEST6
    echo 'TEST 6: after addition of a 1M file'
    #initialization
    git clone --recurse-submodules --remote-submodules --progress --depth=1 --no-local $1 &> /dev/null
    cd $REPO_NAME
    get_storage_used . 
    mem_before=$mem
    #modification of the remote repo  
    cd ../$REMOTE
    create_random_file 'sample5' '1M' #adding a 1M file
    git add sample5 
    git commit --quiet -m"fourth 1M sample created"
    cd ../../$REPO_NAME
    #fetching
    git submodule update --init --recursive --force --remote &> /dev/null
    git fetch --progress --tags --prune --prune-tags origin &> /dev/null
    git checkout -f origin/main &> /dev/null
    get_storage_used .
    mem_after=$mem
    mem=$(($mem_after-$mem_before))
    echo "memory usage: +$mem"
    cd ../$REMOTE
    git reset --hard -q HEAD~1
    cd ../..
    rm -rf performance_testing
}
test7(){
    section TEST7
    echo 'TEST 7: after removal of a 1M file'
    #initialization
    git clone --recurse-submodules --remote-submodules --progress --depth=1 --no-local $1 &> /dev/null
    cd $REPO_NAME
    get_storage_used .
    mem_before=$mem
    #modification of the remote repo  
    cd ../$REMOTE
    rm sample0
    git add sample0 
    git commit --quiet -m"1M sample0 deleted"
    cd ../../$REPO_NAME
    #fetching
    git submodule update --init --recursive --force --remote &> /dev/null
    git fetch --progress --tags --prune --prune-tags origin &> /dev/null
    git checkout -f origin/main &> /dev/null
    get_storage_used .
    mem_after=$mem
    mem=$(($mem_after-$mem_before))
    echo "memory usage: $mem"
    cd ../$REMOTE
    git reset --hard -q HEAD~1
    git clean -df
    cd ../..
    rm -rf performance_testing
}
test8(){
    section TEST8
    echo 'TEST 8: after addition then removal of a 1M file'
    #initialization
    git clone --recurse-submodules --remote-submodules --progress --depth=1 --no-local $1 &> /dev/null
    cd $REPO_NAME
    get_storage_used .
    mem_before=$mem 
    #modification of the remote repo  
    cd ../$REMOTE
    create_random_file 'sample5' '1M' #adding a 1M file
    git add sample5 
    git commit --quiet -m"fourth 1M sample created"
    rm sample5
    git add sample5
    git commit --quiet -m"1M "sample5" deleted"
    cd ../../$REPO_NAME
    #fetching
    git submodule update --init --recursive --force --remote &> /dev/null
    git fetch --progress --tags --prune --prune-tags origin &> /dev/null
    git checkout -f origin/main &> /dev/null
    get_storage_used .
    mem_after=$mem
    mem=$(($mem_after-$mem_before))
    echo "memory usage: $mem"
    cd ../$REMOTE
    git reset --hard -q HEAD~2
    cd ../..
    rm -rf performance_testing
}

#fetching with --depth=1
test9(){
    section TEST9
    echo 'TEST 9: after addition of a 1M file'
    #initialization
    git clone --recurse-submodules --remote-submodules --progress --depth=1 --no-local $1 &> /dev/null
    cd $REPO_NAME
    get_storage_used .
    mem_before=$mem
    #modification of the remote repo  
    cd ../$REMOTE
    create_random_file 'sample5' '1M' #adding a 1M file
    git add sample5 
    git commit --quiet -m"fourth 1M sample created"
    cd ../../$REPO_NAME
    git submodule update --init --recursive --force --depth=1 --remote &> /dev/null
    git fetch --progress --tags --depth=1 --prune --prune-tags origin &> /dev/null
    git checkout -f origin/main &> /dev/null
    get_storage_used .
    mem_after=$mem
    mem=$(($mem_after-$mem_before))
    echo "memory usage: +$mem"
    cd ../$REMOTE
    git reset --hard -q HEAD~1 
    cd ../..
    rm -rf performance_testing
}
test10(){
    section TEST10
    echo 'TEST 10: after removal of a 1M file'
    #initialization 
    git clone --recurse-submodules --remote-submodules --progress --depth=1 --no-local $1 &> /dev/null
    cd $REPO_NAME
    get_storage_used .
    mem_before=$mem
    #modification of the remote repo  
    cd ../$REMOTE
    rm sample0
    git add sample0
    git commit --quiet -m"1M sample0 deleted"
    cd ../../$REPO_NAME
    #fetching
    git submodule update --init --recursive --force --depth=1 --remote &> /dev/null
    git fetch --progress --tags --depth=1 --prune --prune-tags origin &> /dev/null
    git checkout -f origin/main &> /dev/null
    get_storage_used .
    mem_after=$mem
    mem=$(($mem_after-$mem_before))
    echo "memory usage: $mem"
    cd ../$REMOTE
    git reset --hard -q HEAD~1
    cd ../..
    rm -rf performance_testing
}
test11(){
    section TEST11
    echo 'TEST 11: after addition then removal of a 1M file'
    git clone --recurse-submodules --remote-submodules --progress --depth=1 --no-local $1 &> /dev/null
    cd $REPO_NAME
    get_storage_used .
    mem_before=$mem
    #modification of the remote repo  
    cd ../$REMOTE
    create_random_file 'sample5' '1M' #adding a 1M file
    git add sample5 
    git commit --quiet -m"fourth 1M sample created"
    rm sample5
    git add sample5
    git commit --quiet -m"1M "sample5" deleted"
    cd ../../$REPO_NAME
    #fetching
    git submodule update --init --recursive --force --depth=1 --remote &> /dev/null
    git fetch --progress --tags --depth=1 --prune --prune-tags origin &> /dev/null
    git checkout -f origin/main &> /dev/null
    get_storage_used .
    mem_after=$mem
    mem=$(($mem_after-$mem_before))
    echo "memory usage: $mem"
    cd ../$REMOTE
    git reset --hard -q HEAD~2
    cd ../..
    rm -rf performance_testing
}

# --depth=1 fetching with reflog and gc
test12(){
    section TEST12
    echo 'TEST 12: after addition of a 1M file'
    #initialization
    git clone --recurse-submodules --remote-submodules --progress --depth=1 --no-local $1 &> /dev/null
    cd $REPO_NAME
    get_storage_used .
    mem_before=$mem
    #modification of the remote repo  
    cd ../$REMOTE
    create_random_file 'sample5' '1M' #adding a 1M file
    git add sample5 
    git commit --quiet -m"fourth 1M sample created"
    cd ../../$REPO_NAME
    git submodule update --init --recursive --force --depth=1 --remote &> /dev/null
    git fetch --progress --tags --depth=1 --prune --prune-tags origin &> /dev/null
    git checkout -f origin/main &> /dev/null
    git reflog expire --expire=now --all &> /dev/null
    git gc --aggressive --prune=now &> /dev/null
    get_storage_used .
    mem_after=$mem
    mem=$(($mem_after-$mem_before))
    echo "memory usage: +$mem"
    cd ../$REMOTE
    git reset --hard -q HEAD~1 
    cd ../..
    rm -rf performance_testing
}
test13(){
    section TEST13
    echo 'TEST 13: after removal of a 1M file'
    #initialization 
    git clone --recurse-submodules --remote-submodules --progress --depth=1 --no-local $1 &> /dev/null
    cd $REPO_NAME
    get_storage_used .
    mem_before=$mem
    #modification of the remote repo  
    cd ../$REMOTE
    rm sample0
    git add sample0
    git commit --quiet -m"1M sample0 deleted"
    cd ../../$REPO_NAME
    #fetching
    git submodule update --init --recursive --force --depth=1 --remote &> /dev/null
    git fetch --progress --tags --depth=1 --prune --prune-tags origin &> /dev/null
    git checkout -f origin/main &> /dev/null
    git reflog expire --expire=now --all &> /dev/null
    git gc --aggressive --prune=now &> /dev/null
    get_storage_used .
    mem_after=$mem
    mem=$(($mem_after-$mem_before))
    echo "memory usage: $mem"
    cd ../$REMOTE
    git reset --hard -q HEAD~1
    cd ../..
    rm -rf performance_testing
}
test14(){
    section TEST14
    echo 'TEST 14: after addition then removal of a 1M file'
    git clone --recurse-submodules --remote-submodules --progress --depth=1 --no-local $1 &> /dev/null
    cd $REPO_NAME
    get_storage_used .
    mem_before=$mem
    #modification of the remote repo  
    cd ../$REMOTE
    create_random_file 'sample5' '1M' #adding a 1M file
    git add sample5 
    git commit --quiet -m"fourth 1M sample created"
    rm sample5
    git add sample5
    git commit --quiet -m"1M "sample5" deleted"
    cd ../../$REPO_NAME
    #fetching
    git submodule update --init --recursive --force --depth=1 --remote &> /dev/null
    git fetch --progress --tags --depth=1 --prune --prune-tags origin &> /dev/null
    git checkout -f origin/main &> /dev/null
    git reflog expire --expire=now --all &> /dev/null
    git gc --aggressive --prune=now &> /dev/null
    get_storage_used .
    mem_after=$mem
    mem=$(($mem_after-$mem_before))
    echo "memory usage: $mem"
    cd ../$REMOTE
    git reset --hard -q HEAD~2
    cd ../..
    rm -rf performance_testing
}

#--depth=1 fetching with reset --hard
test15(){
    section TEST15
    echo 'TEST 15: after addition of a 1M file'
    #initialization
    git clone --recurse-submodules --remote-submodules --progress --depth=1 --no-local $1 &> /dev/null
    cd $REPO_NAME
    get_storage_used .
    mem_before=$mem
    #modification of the remote repo  
    cd ../$REMOTE
    create_random_file 'sample5' '1M' #adding a 1M file
    git add sample5 
    git commit --quiet -m"fourth 1M sample created"
    cd ../../$REPO_NAME
    git submodule update --init --recursive --force --depth=1 --remote &> /dev/null
    git fetch --progress --tags --depth=1 --prune --prune-tags origin &> /dev/null
    git reset --hard origin/main &> /dev/null
    get_storage_used .
    mem_after=$mem
    mem=$(($mem_after-$mem_before))
    echo "memory usage: +$mem"
    cd ../$REMOTE
    git reset --hard -q HEAD~1 
    cd ../..
    rm -rf performance_testing
}
test16(){
    section TEST16
    echo 'TEST 16: after removal of a 1M file'
    #initialization 
    git clone --recurse-submodules --remote-submodules --progress --depth=1 --no-local $1 &> /dev/null
    cd $REPO_NAME
    get_storage_used .
    mem_before=$mem
    #modification of the remote repo  
    cd ../$REMOTE
    rm sample0
    git add sample0
    git commit --quiet -m"1M sample0 deleted"
    cd ../../$REPO_NAME
    #fetching
    git submodule update --init --recursive --force --depth=1 --remote &> /dev/null
    git fetch --progress --tags --depth=1 --prune --prune-tags origin &> /dev/null
    git reset --hard origin/main &> /dev/null
    get_storage_used .
    mem_after=$mem
    mem=$(($mem_after-$mem_before))
    echo "memory usage: $mem"
    cd ../$REMOTE
    git reset --hard -q HEAD~1
    cd ../..
    rm -rf performance_testing
}
test17(){
    section TEST17
    echo 'TEST 17: after addition then removal of a 1M file'
    git clone --recurse-submodules --remote-submodules --progress --depth=1 --no-local $1 &> /dev/null
    cd $REPO_NAME
    get_storage_used .
    mem_before=$mem
    #modification of the remote repo  
    cd ../$REMOTE
    create_random_file 'sample5' '1M' #adding a 1M file
    git add sample5 
    git commit --quiet -m"fourth 1M sample created"
    rm sample5
    git add sample5
    git commit --quiet -m"1M "sample5" deleted"
    cd ../../$REPO_NAME
    #fetching
    git submodule update --init --recursive --force --depth=1 --remote &> /dev/null
    git fetch --progress --tags --depth=1 --prune --prune-tags origin &> /dev/null
    git reset --hard origin/main &> /dev/null
    get_storage_used .
    mem_after=$mem
    mem=$(($mem_after-$mem_before))
    echo "memory usage: $mem"
    cd ../$REMOTE
    git reset --hard -q HEAD~2
    cd ../..
    rm -rf performance_testing
}
#--depth=1 fetching with reset --hard and reflog and gc
test18(){
    section TEST18
    echo 'TEST 18: after addition of a 1M file'
    git clone --recurse-submodules --remote-submodules --progress --depth=1 --no-local $1 &> /dev/null
    cd $REPO_NAME
    get_storage_used .
    mem_before=$mem
    #modification of the remote repo  
    cd ../$REMOTE
    create_random_file 'sample5' '1M' #adding a 1M file
    git add sample5 
    git commit --quiet -m"fourth 1M sample created"
    cd ../../$REPO_NAME
    #fetching
    git submodule update --init --recursive --force --depth=1 --remote &> /dev/null
    git fetch --progress --tags --depth=1 --prune --prune-tags origin &> /dev/null
    git reset --hard origin/main &> /dev/null
    git reflog expire --expire=now --all &> /dev/null
    git gc --aggressive --prune=now &> /dev/null
    get_storage_used .
    mem_after=$mem
    mem=$(($mem_after-$mem_before))
    echo "memory usage: $mem"
    cd ../$REMOTE
    git reset --hard -q HEAD~1
    cd ../..
    rm -rf performance_testing
}
test19(){
    section TEST19
    echo 'TEST 19: after removal of a 1M file'
    #initialization 
    git clone --recurse-submodules --remote-submodules --progress --depth=1 --no-local $1 &> /dev/null
    cd $REPO_NAME
    get_storage_used .
    mem_before=$mem
    #modification of the remote repo  
    cd ../$REMOTE
    rm sample0
    git add sample0
    git commit --quiet -m"1M sample0 deleted"
    cd ../../$REPO_NAME
    #fetching
    git submodule update --init --recursive --force --depth=1 --remote &> /dev/null
    git fetch --progress --tags --depth=1 --prune --prune-tags origin &> /dev/null
    git reset --hard origin/main &> /dev/null
    git reflog expire --expire=now --all &> /dev/null
    git gc --aggressive --prune=now &> /dev/null
    get_storage_used .
    mem_after=$mem
    mem=$(($mem_after-$mem_before))
    echo "memory usage: $mem"
    cd ../$REMOTE
    git reset --hard -q HEAD~1
    cd ../..
    rm -rf performance_testing
}
test20(){
    section TEST20
    echo 'TEST 20: after addition then removal of a 1M file'
    git clone --recurse-submodules --remote-submodules --progress --depth=1 --no-local $1 &> /dev/null
    cd $REPO_NAME
    get_storage_used .
    mem_before=$mem
    #modification of the remote repo  
    cd ../$REMOTE
    create_random_file 'sample5' '1M' #adding a 1M file
    git add sample5 
    git commit --quiet -m"fourth 1M sample created"
    rm sample5
    git add sample5
    git commit --quiet -m"1M "sample5" deleted"
    cd ../../$REPO_NAME
    #fetching
    git submodule update --init --recursive --force --depth=1 --remote &> /dev/null
    git fetch --progress --tags --depth=1 --prune --prune-tags origin &> /dev/null
    git reset --hard origin/main &> /dev/null
    git reflog expire --expire=now --all &> /dev/null
    git gc --aggressive --prune=now &> /dev/null
    get_storage_used .
    mem_after=$mem
    mem=$(($mem_after-$mem_before))
    echo "memory usage: $mem"
    cd ../$REMOTE
    git reset --hard -q HEAD~2
    cd ../..
    rm -rf performance_testing
}
#--depth=1 fetching after modification applied in submodule
test21(){
    section TEST21
    echo 'TEST 21: after addition of a 1M file'
    if [ "$WITH_SUBMODULE" = "true" ]; then
    git clone --recurse-submodules --remote-submodules --progress --depth=1 --no-local $1 &> /dev/null
        cd $REPO_NAME
        get_storage_used .
        mem_before=$mem
        #modification of the remote submodule  
        cd ..
        cd $REPO_PATH/$SUB_NAME
        create_random_file 'sub_sample1' '1M'
        git add sub_sample1
        git commit --quiet -m"second 1M sample created"
        cd ../../$REPO_NAME
        #fetching
        git submodule update --init --recursive --force --depth=1 --remote &> /dev/null
        git fetch --progress --tags --depth=1 --prune --prune-tags origin &> /dev/null
        git checkout -f origin/main &> /dev/null
        get_storage_used .
        mem_after=$mem
        mem=$(($mem_after-$mem_before))
        echo "memory usage: $mem"
        cd ../$REPO_PATH/$SUB_NAME
        git reset --hard -q HEAD~1
        cd ../..
        rm -rf performance_testing
    else 
        echo "This test will not be performed because we are in no-submodule mode. Change boolean \$WITH_SUBMODULE to switch."
    fi
}

test22(){
    section TEST22
    echo 'TEST 22: after removal of a 1M file'
    if [ "$WITH_SUBMODULE" = "true" ]; then
        git clone --recurse-submodules --remote-submodules --progress --depth=1 --no-local $1 &> /dev/null
        cd $REPO_NAME
        get_storage_used .
        mem_before=$mem
        #modification of the remote submodule  
        cd ..
        cd $REPO_PATH/$SUB_NAME
        rm sub_sample0
        git add sub_sample0
        git commit --quiet -m"1M 'sub_sample0' deleted"
        cd ../../$REPO_NAME
        #fetching
        git submodule update --init --recursive --force --depth=1 --remote &> /dev/null
        git fetch --progress --tags --depth=1 --prune --prune-tags origin &> /dev/null
        git checkout -f origin/main &> /dev/null
        get_storage_used .
        mem_after=$mem
        mem=$(($mem_after-$mem_before))
        echo "memory usage: $mem"
        cd ../$REPO_PATH/$SUB_NAME
        git reset --hard -q HEAD~1
        cd ../..
        rm -rf performance_testing
    else 
        echo "This test will not be performed because we are in no-submodule mode. Change boolean \$WITH_SUBMODULE to switch."
    fi
}

test23(){
    section TEST23
    echo 'TEST 23: after addition then removal of a 1M file'
    if [ "$WITH_SUBMODULE" = "true" ]; then
        git clone --recurse-submodules --remote-submodules --progress --depth=1 --no-local $1 &> /dev/null
        cd $REPO_NAME
        get_storage_used .
        mem_before=$mem
        #modification of the remote submodule  
        cd ..
        cd $REPO_PATH/$SUB_NAME
        create_random_file 'sub_sample1' '1M'
        git add sub_sample1
        git commit --quiet -m"second 1M sample created"
        rm sub_sample1
        git add sub_sample1
        git commit --quiet -m"1M 'sub_sample1' deleted"
        cd ../../$REPO_NAME
        #fetching
        git submodule update --init --recursive --force --depth=1 --remote &> /dev/null
        git fetch --progress --tags --depth=1 --prune --prune-tags origin &> /dev/null
        git checkout -f origin/main &> /dev/null
        get_storage_used .
        mem_after=$mem
        mem=$(($mem_after-$mem_before))
        echo "memory usage: $mem"
        cd ../$REPO_PATH/$SUB_NAME
        git reset --hard -q HEAD~2
        cd ../..
        rm -rf performance_testing
    else 
        echo "This test will not be performed because we are in no-submodule mode. Change boolean \$WITH_SUBMODULE to switch."
    fi
}

# --depth=1 fetching+merging -X theirs with reflog and gc
test24(){
    section TEST24
    echo 'TEST 24: after addition of a 1M file'
    #initialization
    git clone --recurse-submodules --remote-submodules --progress --depth=1 --no-local $1 &> /dev/null
    cd $REPO_NAME
    get_storage_used .
    mem_before=$mem
    #modification of the remote repo  
    cd ../$REMOTE
    create_random_file 'sample5' '1M' #adding a 1M file
    git add sample5 
    git commit --quiet -m"fourth 1M sample created"
    cd ../../$REPO_NAME
    git submodule update --init --recursive --force --depth=1 --remote &> /dev/null
    git fetch --progress --tags --depth=1 --prune --prune-tags origin &> /dev/null
    git merge -X theirs --allow-unrelated-histories &> /dev/null
    git reflog expire --expire=now --all &> /dev/null
    git gc --aggressive --prune=now &> /dev/null
    get_storage_used .
    mem_after=$mem
    mem=$(($mem_after-$mem_before))
    echo "memory usage: +$mem"
    cd ../$REMOTE
    git reset --hard -q HEAD~1 
    cd ../..
    rm -rf performance_testing
}
test25(){
    section TEST25
    echo 'TEST 25: after removal of a 1M file'
    #initialization 
    git clone --recurse-submodules --remote-submodules --progress --depth=1 --no-local $1 &> /dev/null
    cd $REPO_NAME
    get_storage_used .
    mem_before=$mem
    #modification of the remote repo  
    cd ../$REMOTE
    rm sample0
    git add sample0
    git commit --quiet -m"1M sample0 deleted"
    cd ../../$REPO_NAME
    #fetching
    git submodule update --init --recursive --force --depth=1 --remote &> /dev/null
    git fetch --progress --tags --depth=1 --prune --prune-tags origin &> /dev/null
    git merge -X theirs --allow-unrelated-histories &> /dev/null
    git reflog expire --expire=now --all &> /dev/null
    git gc --aggressive --prune=now &> /dev/null
    get_storage_used .
    mem_after=$mem
    mem=$(($mem_after-$mem_before))
    echo "memory usage: $mem"
    cd ../$REMOTE
    git reset --hard -q HEAD~1
    cd ../..
    rm -rf performance_testing
}
test26(){
    section TEST26
    echo 'TEST 26: after addition then removal of a 1M file'
    git clone --recurse-submodules --remote-submodules --progress --depth=1 --no-local $1 &> /dev/null
    cd $REPO_NAME
    get_storage_used .
    mem_before=$mem
    #modification of the remote repo  
    cd ../$REMOTE
    create_random_file 'sample5' '1M' #adding a 1M file
    git add sample5 
    git commit --quiet -m"fourth 1M sample created"
    rm sample5
    git add sample5
    git commit --quiet -m"1M "sample5" deleted"
    cd ../../$REPO_NAME
    #fetching
    git submodule update --init --recursive --force --depth=1 --remote &> /dev/null
    git fetch --progress --tags --depth=1 --prune --prune-tags origin &> /dev/null
    git merge -X theirs --allow-unrelated-histories &> /dev/null
    git reflog expire --expire=now --all &> /dev/null
    git gc --aggressive --prune=now &> /dev/null
    get_storage_used .
    mem_after=$mem
    mem=$(($mem_after-$mem_before))
    echo "memory usage: $mem"
    cd ../$REMOTE
    git reset --hard -q HEAD~2
    cd ../..
    rm -rf performance_testing
}

# --depth=1 fetching+merging -s ours with reflog and gc
test27(){
    section TEST27
    echo 'TEST 27: after addition of a 1M file'
    #initialization
    git clone --recurse-submodules --remote-submodules --progress --depth=1 --no-local $1 &> /dev/null
    cd $REPO_NAME
    get_storage_used .
    mem_before=$mem
    #modification of the remote repo  
    cd ../$REMOTE
    create_random_file 'sample5' '1M' #adding a 1M file
    git add sample5 
    git commit --quiet -m"fourth 1M sample created"
    cd ../../$REPO_NAME
    git submodule update --init --recursive --force --depth=1 --remote &> /dev/null
    git fetch --progress --tags --depth=1 --prune --prune-tags origin &> /dev/null
    git switch -c temp origin/main &> /dev/null #creating a temporary branch identical to origin/main
    git merge -s ours --allow-unrelated-histories main &> /dev/null
    git checkout main &> /dev/null
    git merge --allow-unrelated-histories temp &> /dev/null
    git branch -D temp &> /dev/null
    git reflog expire --expire=now --all &> /dev/null
    git gc --aggressive --prune=now &> /dev/null
    get_storage_used .
    mem_after=$mem
    mem=$(($mem_after-$mem_before))
    echo "memory usage: +$mem"
    cd ../$REMOTE
    git reset --hard -q HEAD~1 
    cd ../..
    rm -rf performance_testing
}
test28(){
    section TEST28
    echo 'TEST 28: after removal of a 1M file'
    #initialization 
    git clone --recurse-submodules --remote-submodules --progress --depth=1 --no-local $1 &> /dev/null
    cd $REPO_NAME
    get_storage_used .
    mem_before=$mem
    #modification of the remote repo  
    cd ../$REMOTE
    rm sample0
    git add sample0
    git commit --quiet -m"1M sample0 deleted"
    cd ../../$REPO_NAME
    #fetching
    git submodule update --init --recursive --force --depth=1 --remote &> /dev/null
    git fetch --progress --tags --depth=1 --prune --prune-tags origin &> /dev/null
    git switch -c temp origin/main &> /dev/null #creating a temporary branch identical to origin/main
    git merge -s ours --allow-unrelated-histories main &> /dev/null
    git checkout main &> /dev/null
    git merge --allow-unrelated-histories temp &> /dev/null
    git branch -D temp &> /dev/null
    git reflog expire --expire=now --all &> /dev/null
    git gc --aggressive --prune=now &> /dev/null
    get_storage_used .
    mem_after=$mem
    mem=$(($mem_after-$mem_before))
    echo "memory usage: $mem"
    cd ../$REMOTE
    git reset --hard -q HEAD~1
    cd ../..
    rm -rf performance_testing
}
test29(){
    section TEST29
    echo 'TEST 29: after addition then removal of a 1M file'
    git clone --recurse-submodules --remote-submodules --progress --depth=1 --no-local $1 &> /dev/null
    cd $REPO_NAME
    get_storage_used .
    mem_before=$mem
    #modification of the remote repo  
    cd ../$REMOTE
    create_random_file 'sample5' '1M' #adding a 1M file
    git add sample5 
    git commit --quiet -m"fourth 1M sample created"
    rm sample5
    git add sample5
    git commit --quiet -m"1M "sample5" deleted"
    cd ../../$REPO_NAME
    #fetching
    git submodule update --init --recursive --force --depth=1 --remote &> /dev/null
    git fetch --progress --tags --depth=1 --prune --prune-tags origin &> /dev/null
    git switch -c temp origin/main &> /dev/null #creating a temporary branch identical to origin/main
    git merge -s ours --allow-unrelated-histories main &> /dev/null
    git checkout main &> /dev/null
    #git merge --allow-unrelated-histories temp &> /dev/null
    git branch -D temp &> /dev/null
    git reflog expire --expire=now --all &> /dev/null
    git gc --aggressive --prune=now &> /dev/null
    get_storage_used .
    mem_after=$mem
    mem=$(($mem_after-$mem_before))
    echo "memory usage: $mem"
    cd ../$REMOTE
    git reset --hard -q HEAD~2
    cd ../..
    rm -rf performance_testing
}


while getopts ":hn:a" option; do
   case $option in
      h) # display Help
            Help
            exit;;
      c) # clean
            clean
      n) # perform a specific test
            TEST_NUM=$OPTARG;;
      a) # perform all tests
            ALL_TESTS=true;; 
     \?) # invalid option
            echo "Error: Invalid option"
            Help
            exit;;
   esac
done

if [ "$ALL_TESTS" = "true" ]; then
    test0 $REMOTE
    test1 $REMOTE
    test2 $REMOTE
    test3 $REMOTE
    test4 $REMOTE
    test5 $REMOTE
    echo $(section "Tests on the updating of the repository")
    section "classic fetching+checking out"
    test6 $REMOTE
    test7 $REMOTE
    test8 $REMOTE
    echo $(section "fetching+checking out with --depth=1")
    test9 $REMOTE
    test10 $REMOTE
    test11 $REMOTE
    echo $(section "--depth=1 fetching+checking out reflog and gc")
    test12 $REMOTE
    test13 $REMOTE
    test14 $REMOTE
    echo $(section "--depth=1 fetching+ reset --hard")
    test15 $REMOTE
    test16 $REMOTE
    test17 $REMOTE
    echo $(section "--depth=1 fetching+ reset --hard and reflog and gc")
    test18 $REMOTE
    test19 $REMOTE
    test20 $REMOTE
    echo $(section "--depth=1 fetching+checking out after modification applied in submodule")
    test21 $REMOTE
    test22 $REMOTE
    test23 $REMOTE
    echo $(section "--depth=1 fetching+merging -X theirs with reflog and gc")
    test24 $REMOTE
    test25 $REMOTE
    test26 $REMOTE
    echo $(section "--depth=1 fetching+merging -s ours with reflog and gc")
    test27 $REMOTE
    test28 $REMOTE
    test29 $REMOTE


elif [ -n "$TEST_NUM" ]; then
    case $TEST_NUM in
        0) 
            test0 $REMOTE;;
        1)
            test1 $REMOTE;;
        2) 
            test2 $REMOTE;;
        3) 
            test3 $REMOTE;;
        4) 
            test4 $REMOTE;;
        5) 
            test5 $REMOTE;;
        6)
            test6 $REMOTE;;
        7) 
            test7 $REMOTE;;
        8) 
            test8 $REMOTE;;
        9)
            test9 $REMOTE;;
        10)
            test10 $REMOTE;;
        11)
            test11 $REMOTE;;
        12)
            test12 $REMOTE;;
        13)     
            test13 $REMOTE;;
        14)
            test14 $REMOTE;;
        15)
            test15 $REMOTE;;
        16)
            test16 $REMOTE;;
        17)
            test17 $REMOTE;;
        18)     
            test18 $REMOTE;;
        19)
            test19 $REMOTE;;
        20)
            test20 $REMOTE;;
        21)
            test21 $REMOTE;;
        22)
            test22 $REMOTE;;
        23)
            test23 $REMOTE;;
        24)
            test24 $REMOTE;;
        25)
            test25 $REMOTE;;
        26)
            test26 $REMOTE;;
        26)
            test26 $REMOTE;;
        27)
            test27 $REMOTE;;
        28)
            test28 $REMOTE;;
        29)
            test29 $REMOTE;;
        *)
            echo "Error: Invalid test number"
            exit;;
    esac
fi